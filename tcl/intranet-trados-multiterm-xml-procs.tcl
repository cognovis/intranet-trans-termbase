# ---------------------------------------------------------------
# Procedures to handle Trados Multiterm
# ---------------------------------------------------------------

namespace eval intranet_termbase:: {}
namespace eval intranet_termbase::multiterm:: {}
namespace eval intranet_termbase::multiterm::xml:: {}


ad_proc -public intranet_termbase::multiterm::xml::import_termbase {
	-xml_path
	-tb_path
	-project_id
	{-overwrite_p "0"}
	{-termbase_id ""}
	{-company_id ""}
} {
	Creates a new termbase from the exported XML. Returns the termbase_id
} {
	
	db_1row project_info "select to_char(creation_date, 'YYYY-MM-DD HH24:MI:SS') as project_start_date from acs_objects where object_id = :project_id"
	
	set termbase_infos [list]
	if {$termbase_id eq ""} {
		set termbase_id [db_string termbase "select termbase_id from im_termbases where company_id = :company_id and termbase_name = :termbase_name" -default ""]	
	}
	
	if {$termbase_id eq ""} {
		lappend termbase_infos "Creating a new Termbase for [im_name_from_id $company_id]"
		set termbase_id [intranet_termbase::multiterm::create_termbase -tb_path $tb_path -company_id $company_id]
	} else {
		# make sure we have the descriptions....
		lappend termbase_infos "Updating existing Termbase for [im_name_from_id $company_id] with $tb_path"
		intranet_termbase::multiterm::import_description_type -tb_path $tb_path
	}
	
	set language_ids [intranet_termbase::multiterm::language_mapping -termbase_id $termbase_id -array_name language]
	set main_language_id [lindex $language_ids 0]

	set xml [new_CkXml]
	CkXml_put_Encoding $xml "utf-8"
	CkXml_put_Utf8 $xml 1

	set success [CkXml_LoadXmlFile $xml $xml_path]
	if {[expr $success != 1]} then {
		puts [CkXml_lastErrorText $xml]
		delete_CkXml $xml
		exit
	}

	# Loop through all conceptGrps
	set numConcepts [CkXml_NumChildrenHavingTag $xml "conceptGrp"]
	set concept_ids [list]
	for {set i 0} {$i <= [expr $numConcepts - 1]} {incr i} {
		#  Navigate to the Nth child.
		set success [CkXml_GetNthChildWithTag2 $xml "conceptGrp" $i]
		
		# ---------------------------------------------------------------
		# Concept data
		# ---------------------------------------------------------------

		set success [CkXml_FindChild2 $xml "concept"]
		set concept_nr [CkXml_content $xml]
		set success [CkXml_GetParent2 $xml]
		set transac_list [intranet_termbase::multiterm::xml::get_transacGrp -xml $xml -context "CONCEPT:: $concept_nr"]
		set description_list [intranet_termbase::multiterm::xml::get_descripGrp -xml $xml -transac_list $transac_list]
		
		# Import concept
		set concept_info [intranet_termbase::import_concept\
			-concept_nr $concept_nr\
			-termbase_id $termbase_id\
			-creation_user [intranet_termbase::user_id -termbase_user [lindex $transac_list 0]]\
			-creation_date [split [lindex $transac_list 1] "T"]\
			-modifying_user [intranet_termbase::user_id -termbase_user [lindex $transac_list 2]]\
			-modification_date [split [lindex $transac_list 3] "T"]\
			-overwrite_p $overwrite_p \
			-project_start_date $project_start_date
		]

		set concept_id [lindex $concept_info 0]
		lappend concept_ids $concept_id
		set mode [lindex $concept_info 1]
		
		# Get the list of messages for the concept
		set concept_infos [lindex $concept_info 2]
		set termbase_infos [list {*}$termbase_infos {*}$concept_infos]
		
		# ---------------------------------------------------------------
		# Figure out what exactly to do with different concepts
		# ---------------------------------------------------------------
		
		if {$mode eq "different"} {
			# Try to find a match for the concept based on the terms
			set match_language_ids [list]
			set match_concept_ids [list]
			set numLanguages [CkXml_NumChildrenHavingTag $xml "languageGrp"]
			for {set j 0} {$j <= [expr $numLanguages -1]} {incr j} {
				CkXml_GetNthChildWithTag2 $xml "languageGrp" $j
			
				# Get the language_id from the language node
				CkXml_FindChild2 $xml "language"
				set lang [CkXml_getAttrValue $xml type]
				set locale [CkXml_getAttrValue $xml lang]
				CkXml_GetParent2 $xml
			
				set main_locale [string range $locale 0 1]
				if {[info exists language($lang)]} {
					set language_id $language($lang)
				} elseif {[info exists language($locale)]} {
					set language_id $language($locale)
				} elseif {[info exists language($main_locale)]} {
					set language_id $language($main_locale)
				} else {
					continue
				}
				
				lappend match_language_ids $language_id
				
				# ---------------------------------------------------------------
				# Loop through the terms
				# ---------------------------------------------------------------
				
				set numTerms [CkXml_NumChildrenHavingTag $xml "termGrp"]
				
				for  {set k 0} {$k <= [expr $numTerms -1]} {incr k} {
				 	CkXml_GetNthChildWithTag2 $xml "termGrp" $k
				 	
				 	# Find the actual term from the node
				 	CkXml_FindChild2 $xml "term"
					set term_nr $k
					set term [CkXml_content $xml]
					CkXml_GetParent2 $xml
					
					set match_concepts($language_id) [db_list matched "select itc.concept_id from im_tb_terms itt, im_tb_concepts itc where itc.concept_id = itt.concept_id and term = :term and termbase_id = :termbase_id and language_id = :language_id"]
					ns_log Notice "Matched $match_concepts($language_id) for term $term and concept_nr $concept_nr"
					CkXml_GetParent2 $xml
				}		
				CkXml_GetParent2 $xml		
			}
			
			# ---------------------------------------------------------------
			# Now record the matches
			# ---------------------------------------------------------------
			foreach language_id $match_language_ids {
				foreach match_concept_id $match_concepts($language_id) {
					if {[info exists match_counter($match_concept_id)]} {
						incr match_counter($match_concept_id)
					} else {
						set match_counter($match_concept_id) 1
					}
					if {[lsearch $match_concept_ids $match_concept_id]<0} {
						lappend match_concept_ids $match_concept_id
					}
				}
			}
			
			# ---------------------------------------------------------------
			# Create the new concept
			# ---------------------------------------------------------------
			set new_concept_nr [db_string max_concept "select max(concept_nr::integer) from im_tb_concepts where termbase_id = :termbase_id"]
			set new_concept_nr [expr $new_concept_nr +1]

			set match_creation_user [intranet_termbase::user_id -termbase_user [lindex $transac_list 0]]
			set match_creation_date [split [lindex $transac_list 1] "T"]
			set match_modifying_user [intranet_termbase::user_id -termbase_user [lindex $transac_list 2]]
			set match_modification_date [split [lindex $transac_list 3] "T"]

			set concept_id [db_string new_concept "select im_tb_concept__new(null,:match_creation_date,:match_creation_user,:match_modification_date,:match_modifying_user,null,:termbase_id,:new_concept_nr) from dual"]

			set mode "new"
			
			# ---------------------------------------------------------------
			# Deal with the matches
			# It is only a match if all terms are the same
			# Otherwise leave it alone
			# ---------------------------------------------------------------

			foreach match_concept_id $match_concept_ids {
				if {$match_counter($match_concept_id) eq $numLanguages} {
					# Update the old id as matched
					db_dml update_dupe "update im_tb_concepts set concept_status_id = 4352 where concept_id = :match_concept_id"
					
					# Add the relationship
					db_0or1row rels "select acs_rel__new (
			      	   null,             -- rel_id
      	   			   'im_tb_concept_concept_rel',   -- rel_type
			      	   :match_concept_id,      -- object_id_one
			      	   :concept_id,      -- object_id_two
			      	   null,             -- context_id
			      	   :match_creation_user,             -- creation_user
			      	   null             -- creation_ip
			      )"
				}
			}
			
			lappend concept_infos "created new concept $new_concept_nr for original concept $concept_nr."
		}
		
		# ---------------------------------------------------------------
		# We are done with the duplicates, now start the actual creation
		# ---------------------------------------------------------------

		if {$mode ne "ignore"} {
			
			# Update the descriptions
			set description_list [intranet_termbase::multiterm::xml::get_descripGrp -xml $xml -transac_list $transac_list]
		 	set description_infos [intranet_termbase::import_descriptions -concept_id $concept_id -description_list $description_list -mode $mode]
			set termbase_infos [list {*}$termbase_infos {*}$description_infos]

			# ---------------------------------------------------------------
			# Loop through the languages
			# ---------------------------------------------------------------
			set numLanguages [CkXml_NumChildrenHavingTag $xml "languageGrp"]
			for {set j 0} {$j <= [expr $numLanguages -1]} {incr j} {
				CkXml_GetNthChildWithTag2 $xml "languageGrp" $j

				# Get the language_id from the language node
				CkXml_FindChild2 $xml "language"
				set lang [CkXml_getAttrValue $xml type]
				set locale [CkXml_getAttrValue $xml lang]
				CkXml_GetParent2 $xml
				
				set main_locale [string range $locale 0 1]
				if {[info exists language($lang)]} {
					set language_id $language($lang)
				} elseif {[info exists language($locale)]} {
					set language_id $language($locale)
				} elseif {[info exists language($main_locale)]} {
					set language_id $language($main_locale)
				} else {
#					ad_return_error "Can't handle language" "We are unable to handle $lang :: $locale"
				    continue
				}
								
				# Languages can have descriptions
				set description_list [intranet_termbase::multiterm::xml::get_descripGrp -xml $xml -transac_list $transac_list -language_id $language_id]
				set description_infos [intranet_termbase::import_descriptions -concept_id $concept_id -description_list $description_list -mode $mode]
				set termbase_infos [list {*}$termbase_infos {*}$description_infos]
					
				# ---------------------------------------------------------------
				# Loop through the terms
				# ---------------------------------------------------------------
	
				set numTerms [CkXml_NumChildrenHavingTag $xml "termGrp"]
				set term_ids [list]
				for  {set k 0} {$k <= [expr $numTerms -1]} {incr k} {
				 	CkXml_GetNthChildWithTag2 $xml "termGrp" $k
				 	
				 	# Find the actual term from the node
				 	CkXml_FindChild2 $xml "term"
					set term_nr $k
					set term [CkXml_content $xml]
					CkXml_GetParent2 $xml
					
					# Get the transac data
					set transac_list [intranet_termbase::multiterm::xml::get_transacGrp -xml $xml -context "Term:: $term"]
					if {$transac_list ne ""} {
						set term_creation_user [intranet_termbase::user_id -termbase_user [lindex $transac_list 0]]
						set term_creation_date [split [lindex $transac_list 1] "T"]
						set term_modifying_user [intranet_termbase::user_id -termbase_user [lindex $transac_list 2]]
						set term_modification_date [split [lindex $transac_list 3] "T"]
					} 
			
					# Import term
					set term_info [intranet_termbase::import_term\
						-term_nr $term_nr\
						-term $term\
						-concept_id $concept_id\
						-creation_user $term_creation_user\
						-creation_date $term_creation_date\
						-modifying_user $term_modifying_user\
						-modification_date $term_modification_date\
						-language_id $language_id\
						-project_start_date $project_start_date\
						-mode $mode]
						
					set term_id [lindex $term_info 0]
					lappend term_ids $term_id
					set term_infos [lindex $term_info 1]
					set termbase_infos [list {*}$termbase_infos {*}$term_infos]

					set description_list [intranet_termbase::multiterm::xml::get_descripGrp -xml $xml -language_id $language_id -transac_list $transac_list]
					set description_infos [intranet_termbase::import_descriptions -term_id $term_id -description_list $description_list -mode $mode]
					set termbase_infos [list {*}$termbase_infos {*}$description_infos]
					CkXml_GetParent2 $xml
				}
				
				if {$mode ne "new"} {
					# Delete all terms of the concept not imported
					set del_term_ids [db_list to_delete "select term_id from im_tb_terms where concept_id = :concept_id and language_id = :language_id and term_id not in ([template::util::tcl_to_sql_list $term_ids])"]
					foreach del_term_id $del_term_ids {
						db_1row term_info "select term, term_nr, im_name_from_id(language_id) as term_language from im_tb_terms where term_id = :del_term_id"
						db_dml update "update im_tb_terms set modification_date = now(), term_status_id = 4353 where term_id = :del_term_id"
						lappend termbase_infos "Set $term ($term_nr) in language $term_language as deleted"
					}
				}			
				CkXml_GetParent2 $xml
			}
		}
		set success [CkXml_GetParent2 $xml]
	}
	
	# ---------------------------------------------------------------
	# Check for concepts no longer in the upload
	# ---------------------------------------------------------------
	db_foreach to_delete "select concept_id as del_concept_id,concept_nr as del_concept_nr, to_char(modification_date, 'YYYY-MM-DD HH24:MI:SS') as del_modification_date from im_tb_concepts where termbase_id = :termbase_id and concept_id not in ([template::util::tcl_to_sql_list $concept_ids]) and concept_status_id in (4350,4351)" {
		
		set main_language_id [intranet_termbase::main_language -termbase_id $termbase_id]
		set term [db_string term "select term from im_tb_terms where concept_id = :del_concept_id and language_id = :main_language_id limit 1" -default ""]

		if {$overwrite_p} {
			# Delete the concept for good
			lappend termbase_infos "Deleting old concept with concept_id $del_concept_id"
			db_1row delete "select im_tb_concept__delete(:del_concept_id) from dual"
		} else {
			# Check if the project start date is older than the modification_date of the concept
			if {$project_start_date < $del_modification_date} {
				# The deleted entry was modified after project creation in a different project, so do nothing
				lappend termbase_infos "Old concept $del_concept_nr with term $term was modified after project creation by someone else. There it will not be deleted."
			} else {
				# Mark the concept as deleted
				db_dml update "update im_tb_concepts set modification_date = now(), concept_status_id = 4353 where concept_id = :del_concept_id"
				lappend termbase_infos "Marking old concept $del_concept_nr ($del_concept_id) with term $term as deleted"
			}
		}
	}
		
	delete_CkXml $xml
	
	# MOVE the media folder if necessary.
	set path_elements [lrange [file split $xml_path] 0 end-1]
	lappend path_elements "media_[file rootname [file tail $tb_path]]"
	set media_folder [join $path_elements "/"]

	if {[file exists $media_folder]} {
		set trados_folder [im_trans_trados_folder -company_id $company_id]
		exec rsync -rz --delete $media_folder $trados_folder
	}
	
	return $termbase_infos
}

ad_proc -public intranet_termbase::multiterm::xml::get_transacGrp {
	-xml
	{-context ""}
} {
	Returns the transaction information for the current transacGrp
} {
	for {set i 0} {$i <2} {incr i} {
		CkXml_GetNthChildWithTag2 $xml "transacGrp" $i
		CkXml_FindChild2 $xml "transac"
		set type [CkXml_getAttrValue $xml type]
		set user($type) [CkXml_content $xml]
		CkXml_GetParent2 $xml
		CkXml_FindChild2 $xml "date"
		set date($type) [CkXml_content $xml]
		CkXml_GetParent2 $xml
		CkXml_GetParent2 $xml
	}
	
	if {$type ne ""} {
		return [list $user(origination) $date(origination) $user(modification) $date(modification)]	
	} else {
		return ""
	}
}

ad_proc -public intranet_termbase::multiterm::xml::get_descripGrp {
	-xml
	{-language_id ""}
	{-transac_list ""}
} {
	Returns the description information for the current Group
} {
	set numDescrip [CkXml_NumChildrenHavingTag $xml "descripGrp"]
	if {$numDescrip >0} {
		intranet_termbase::description_types -array_name desc_type_id
		
		set description_list [list]
		for {set i 0} {$i <= [expr $numDescrip - 1]} {incr i} {
			CkXml_GetNthChildWithTag2 $xml "descripGrp" $i
			CkXml_FindChild2 $xml "descrip"
			set type [CkXml_getAttrValue $xml type]
						
			set numChilds [CkXml_get_NumChildren $xml]
			
			if {$numChilds >0} {
				# Need to decompress using string builder
				set sb [new_CkStringBuilder]
				CkXml_GetXmlSb $xml $sb
				set description [CkStringBuilder_getBetween $sb "<descrip type=\"$type\">" "</descrip>"]
				delete_CkStringBuilder $sb
			} else {
				set description [CkXml_content $xml]				
			}
	
			CkXml_GetParent2 $xml
			CkXml_GetParent2 $xml
			if {$description ne ""} {
				
				# First make sure we actually have the description type...
				if {![info exists desc_type_id($type)]} {
					keylset desc_keyl description_type_id [intranet_termbase::multiterm::description_type -type $type]
				} else {
					keylset desc_keyl description_type_id $desc_type_id($type)
				}
				keylset desc_keyl description $description
				keylset desc_keyl language_id $language_id
				keylset desc_keyl description_nr $i
				if {$transac_list ne ""} {
					# Append the date info according to acs_objects
					keylset desc_keyl creation_user [intranet_termbase::user_id -termbase_user [lindex $transac_list 0]]\
					keylset desc_keyl creation_date [split [lindex $transac_list 1] "T"]\
					keylset desc_keyl modifying_user [intranet_termbase::user_id -termbase_user [lindex $transac_list 2]]\
					keylset desc_keyl last_modified [split [lindex $transac_list 3] "T"]
				}
				lappend descrip_list $desc_keyl
				unset desc_keyl
			}
		}
		return $descrip_list
	} else {
		return ""
	}
}


# ---------------------------------------------------------------
# Multiterm XML Generation
# ---------------------------------------------------------------
ad_proc -public intranet_termbase::multiterm::export_termbase {
	-project_id
	-xml_path
	{-termbase_id ""}
	{-company_id ""}
} {
	Generates the XML for the termbase
} {
	
	if {$termbase_id eq ""} {
		set termbase_id [intranet_termbase::termbase_id -company_id $company_id]
	}

	set concept_ids [db_list concept_ids "select concept_id from im_tb_concepts where termbase_id = :termbase_id and concept_status_id in (4350,4351) order by cast(concept_nr as integer)"]
	set xml [new_CkXml]
	CkXml_put_Encoding $xml "utf-8"
	CkXml_put_Utf8 $xml 1

	CkXml_put_Tag $xml "mtf"

	foreach concept_id $concept_ids {
		intranet_termbase::multiterm::concept_group_xml -concept_id $concept_id -xml $xml
	}

	CkXml_SaveXml $xml $xml_path
	return [CkXml_getXml $xml]

	delete_CkXml $xml
}


ad_proc -public intranet_termbase::multiterm::concept_group_xml {
	-concept_id
	-xml
} {
	Generates the XML for a concept group
} {

	db_1row concept_info "select concept_nr, termbase_id, to_char(modification_date,'YYYY-MM-DD HH24:MI:SS') as concept_modification_date,modifying_user as concept_modifying_user, to_char(creation_date,'YYYY-MM-DD HH24:MI:SS') as concept_creation_date, creation_user as concept_creation_user from acs_objects o, im_tb_concepts itc where itc.concept_id = o.object_id and concept_id = :concept_id"

	set conceptGrpNode [CkXml_NewChild $xml "conceptGrp" ""]
	CkXml_NewChild2 $conceptGrpNode "concept" $concept_nr

	# Append the description for the conceptGrp

	db_foreach description "select im_category_from_id(description_type_id) as description_type,description from im_tb_descriptions where concept_id = :concept_id and language_id is null" {
		set descripGrpNode [CkXml_NewChild $conceptGrpNode "descripGrp" ""]
		set descripNode [CkXml_NewChild $descripGrpNode "descrip" "$description"]
		CkXml_UpdateAttribute $descripNode "type" "$description_type"
		delete_CkXml $descripGrpNode
	}

	# Get the languages for the concept
	set language_ids [list]
	db_foreach languages "select distinct itl.language_id, lang, type from im_tb_languages itl, im_tb_terms itt where itt.language_id = itl.language_id and itt.concept_id = :concept_id and termbase_id = :termbase_id order by lang" {
		set ${language_id}_languageGrpNode [CkXml_NewChild $conceptGrpNode "languageGrp" ""]
		set languageNode [CkXml_NewChild [set ${language_id}_languageGrpNode] "language" ""]
		CkXml_UpdateAttribute $languageNode "type" "$type"
		CkXml_UpdateAttribute $languageNode "lang" "$lang"
		delete_CkXml $languageNode
		lappend language_ids $language_id

		# Append the description for the languageGrp
		db_foreach description "select im_category_from_id(description_type_id) as description_type,description from im_tb_descriptions where concept_id = :concept_id and language_id = :language_id" {
			set descripGrpNode [CkXml_NewChild [set ${language_id}_languageGrpNode] "descripGrp" ""]
			set descripNode [CkXml_NewChild $descripGrpNode "descrip" "$description"]
			CkXml_UpdateAttribute $descripNode "type" "$description_type"
			delete_CkXml $descripGrpNode
		}
	}

	foreach language_id $language_ids {
		# Get the terms
		db_foreach terms "select term_id, term,to_char(modification_date,'YYYY-MM-DD HH24:MI:SS') as term_modification_date,modifying_user as term_modifying_user, to_char(creation_date,'YYYY-MM-DD HH24:MI:SS') as term_creation_date, creation_user as term_creation_user from acs_objects o, im_tb_terms where language_id = :language_id and concept_id = :concept_id and term_id = object_id and term_status_id in (4350,4351)" {
			set creation_user_pretty [db_string user "select username from users where user_id = :term_creation_user"]
			set modifying_user_pretty [db_string user "select username from users where user_id = :term_modifying_user"]
			set modification_date [join $term_modification_date "T"]
			set creation_date [join $term_creation_date "T"]
			set termGrpNode [CkXml_NewChild [set ${language_id}_languageGrpNode] "termGrp" ""]
			CkXml_NewChild2 $termGrpNode "term" $term


			# Descriptions
			db_foreach description "select im_category_from_id(description_type_id) as description_type,description from im_tb_descriptions where term_id = :term_id" {
				set descripGrpNode [CkXml_NewChild $termGrpNode "descripGrp" ""]
				set descripNode [CkXml_NewChild $descripGrpNode "descrip" "$description"]
				CkXml_UpdateAttribute $descripNode "type" "$description_type"
				delete_CkXml $descripGrpNode
			}
			
			
			# Origination
			set transacGrpNode [CkXml_NewChild $termGrpNode "transacGrp" ""]
			set	transacNode [CkXml_NewChild $transacGrpNode "transac" $creation_user_pretty]
			CkXml_UpdateAttribute $transacNode "type" "origination"
			CkXml_NewChild2 $transacGrpNode "date" $creation_date
			
			# Modification
			set transacGrpNode [CkXml_NewChild $termGrpNode "transacGrp" ""]
			set	transacNode [CkXml_NewChild $transacGrpNode "transac" $modifying_user_pretty]
			CkXml_UpdateAttribute $transacNode "type" "modification"
			CkXml_NewChild2 $transacGrpNode "date" $modification_date
			
			delete_CkXml $transacGrpNode
		}
	}
	
	# ---------------------------------------------------------------
	# Transac Group
	# ---------------------------------------------------------------
	set creation_user_pretty [db_string user "select username from users where user_id = :concept_creation_user"]
	set modifying_user_pretty [db_string user "select username from users where user_id = :concept_modifying_user"]
	set modification_date [join $concept_modification_date "T"]
	set creation_date [join $concept_creation_date "T"]
	
	# Origination
	set transacGrpNode [CkXml_NewChild $conceptGrpNode "transacGrp" ""]
	set	transacNode [CkXml_NewChild $transacGrpNode "transac" $creation_user_pretty]
	CkXml_UpdateAttribute $transacNode "type" "origination"
	CkXml_NewChild2 $transacGrpNode "date" $creation_date
	
	# Modification
	set transacGrpNode [CkXml_NewChild $conceptGrpNode "transacGrp" ""]
	set	transacNode [CkXml_NewChild $transacGrpNode "transac" $modifying_user_pretty]
	CkXml_UpdateAttribute $transacNode "type" "modification"
	CkXml_NewChild2 $transacGrpNode "date" $modification_date
	
	delete_CkXml $transacGrpNode
	
	delete_CkXml $conceptGrpNode
}
