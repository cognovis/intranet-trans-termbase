# ---------------------------------------------------------------
# Procedures to handle Trados Multiterm
# ---------------------------------------------------------------

namespace eval intranet_termbase:: {}
namespace eval intranet_termbase::multiterm:: {}


ad_proc -public intranet_termbase::multiterm::create_termbase {
	-tb_path
	{-company_id ""}
	{-termbase_type_id ""}
	{-termbase_status_id ""}
} {
	Creates a new termbase from the tb_path. Returns the termbase_id
} {
	# Default is the internal company for the termbase
	if {$company_id eq ""} {
		set company_id [im_company_internal]
	}
	
	set termbase_name [intranet_termbase::multiterm::friendly_name -tb_path $tb_path]
	set termbase_id [db_string termbase "select termbase_id from im_termbases where company_id = :company_id and termbase_name = :termbase_name" -default ""]	

	if {$termbase_id eq ""} {		
		# Only create on if needed
		set termbase_id [db_string new_termbase "select im_termbase__new(null,now(),[ad_conn user_id],null,:termbase_name,:company_id) from dual" -default ""]
	}
	
	set language_ids [intranet_termbase::multiterm::language_mapping -tb_path $tb_path -termbase_id $termbase_id -array_name languages]
	
	# Load the descriptions as well
	intranet_termbase::multiterm::import_description_type -tb_path $tb_path	

	return $termbase_id
}

ad_proc -public intranet_termbase::multiterm::language_mapping {
	{-tb_path ""}
	-array_name
	-termbase_id
} {
	Returns the list of languages supported and sets the array
	to return the language_id for the language

	@return List of language_ids if successfully able to map all language, 0 otherwise
} {
	upvar 1 $array_name tb_array
	if {[info exists tb_array]} {
		unset tb_array
	}

	if {$tb_path ne ""} {
		# Import the languages / overwrite them
		db_dml delete_lang_maps "delete from im_tb_languages where termbase_id = :termbase_id"

		# Catch as mdb-export does not work on mounted drives easily
		if { [catch {exec /usr/bin/mdb-export $tb_path mtIndexes} mtIndexes_csv] } {
			set tmp_name "/tmp/[file tail $tb_path]"
			file copy -force $tb_path $tmp_name
			set mtIndexes_csv [exec /usr/bin/mdb-export $tmp_name mtIndexes]
			file delete $tmp_name
		}
		
		# Let's assume all goes fine
		set category_type "Intranet Translation Language"
		foreach line "[im_csv_get_values $mtIndexes_csv ,]" {
			set lang [string toupper [lindex $line 1]]
			set type [lindex $line 0]
			if {$lang ne "" || $type ne ""} {
				set category_id [db_string language "select category_id from im_categories where category_type = :category_type and aux_string1 = :type" -default ""]
				if {$category_id eq ""} {
					# try to find the language_id using the lang
					set category_id [db_string language "select category_id from im_categories where category_type = :category_type and upper(category) = :lang" -default ""]
				}
				if {$category_id eq ""} {
					# Maybe the language actually is a type
					set category_id [db_string language "select category_id from im_categories where category_type = :category_type and upper(category) = :type" -default ""]
				}
				if {$category_id eq ""} {
					# Try with the iso only
					set category_id [db_string language "select category_id from im_categories where category_type = :category_type and upper(category) like '${lang}-' order by sort_order limit 1" -default ""]
				}
				if {$category_id eq ""} {
					# Try with the iso only
					set category_id [db_string language "select category_id from im_categories where category_type = :category_type and upper(category) like '-${lang}' order by sort_order limit 1" -default ""]
				}
				if {$category_id eq ""} {
					# We did not match a language, therefore create it
					set category_id [db_string max_cat_id "select max(category_id) from im_categories" -default 10000]
					set category_id [expr $category_id + 1]
					
					# ---------------------------------------------------------------
					# Update the category
					# ---------------------------------------------------------------
						
					db_dml new_category_entry {
						insert into im_categories (
							category_id, category, category_type
						) values (
							:category_id, :lang, :category_type
						)
					}
					
					# set aux_string1
					db_dml update "update im_categories set aux_string1 = :type where category_id = :category_id"
					callback im_category_after_create -object_id $category_id \
						-status "" -type "" -category_id $category_id \
						-category_type $category_type
		
					ns_log Notice "Created category for language $type and iso-code $lang"
				}
				
				db_dml insert "insert into im_tb_languages (language_id,termbase_id,type,lang) values (:category_id,:termbase_id,:type,:lang)"
			}
		}
	}
	
	set language_ids [list]
	db_foreach tb_languages {select language_id, type, lang from im_tb_languages where termbase_id = :termbase_id} {
		set tb_array($type) $language_id
		set tb_array($lang) $language_id
		set tb_array([im_category_from_id -translate_p 0 $language_id]) $language_id
		lappend language_ids $language_id
	}
	return $language_ids
}

ad_proc -public intranet_termbase::multiterm::import_description_type {
	-tb_path
} {
	Looks at the Possible Description types and creates a new category for them
} {
	# Catch as mdb-export does not work on mounted drives easily
	if { [catch {exec /usr/bin/mdb-export $tb_path mtFields} mtFields_csv] } {
		set tmp_name "/tmp/[file tail $tb_path]"
		file copy -force $tb_path $tmp_name
		set mtFields_csv [exec /usr/bin/mdb-export $tmp_name mtFields]
		file delete $tmp_name
	}


	foreach line "[im_csv_get_values $mtFields_csv ,]" {
		set type [lindex $line 0]

		intranet_termbase::multiterm::description_type -type $type
	}
}


ad_proc -public intranet_termbase::multiterm::description_type {
	-type
} {
	Create a category for the type if necessary

	Returns the category_id
} {
	set category_name $type
	set category_type "Intranet Termbase Description Type"

	# Does the category exist already?
	set category_id [db_string category "select category_id from im_categories where category = :category_name and category_type = :category_type" -default ""]
	if {$category_id eq ""} {
		set category_id [db_string max_cat_id "select max(category_id) from im_categories" -default 10000]
		set category_id [expr $category_id + 1]

		# ---------------------------------------------------------------
		# Update the category
		# ---------------------------------------------------------------
		db_dml new_category_entry {
			insert into im_categories (
				category_id, category, category_type
			) values (
				:category_id, :category_name, :category_type
			)
		}
		callback im_category_after_create -object_id $category_id \
			-status "" -type "" -category_id $category_id \
			-category_type $category_type
	}

	return $category_id
}

ad_proc -public intranet_termbase::multiterm::user_mapping {
	-termbase_id
	-array_name
} {
	Create an array containing the mapping of the user_id to the termbase_user
} {
	
	upvar 1 $array_name user_array
	if {[info exists user_array]} {
			unset user_array
	}

	set termbase_users [db_list users "select distinct value from mtfieldsvalues where termbase_id = :termbase_id and path in ('trG\[tr/@type=''modification''\]/tr','trG\[tr/@type=''origination''\]/tr')"]
	
	foreach termbase_user $termbase_users {
		set user_id [acs_user::get_by_username -username "$termbase_user"]
		if {"" eq $user_id} {
			array set creation_info [auth::create_user \
				-user_id $user_id \
				-verify_password_confirm \
				-username $termbase_user \
				-email "${termbase_user}@foo.com" \
				-first_names "$termbase_user" \
				-last_name "TB USER" \
				-screen_name ""]
			set user_array($termbase_user) $creation_info(user_id)
		} else {
			set user_array($termbase_user) $user_id
		}
	}
}


# ---------------------------------------------------------------
# Generic procs
# ---------------------------------------------------------------


ad_proc -public intranet_termbase::multiterm::friendly_name {
	-tb_path
} {
	Returns the friendly name of the TB for use in Multiterm
} {
	
	# Catch as mdb-export does not work on mounted drives easily
	if { [catch {exec /usr/bin/mdb-export $tb_path mtBlobs} mtBlobs_csv] } {
		set tmp_name "/tmp/[file tail $tb_path]"
		file copy -force $tb_path $tmp_name
		set mtBlobs_csv [exec /usr/bin/mdb-export $tmp_name mtBlobs]
		file delete $tmp_name
	}

	set name ""

	foreach line "[im_csv_get_values $mtBlobs_csv ,]" {
		set type [lindex $line 3]
		set value [lindex $line 2]
		if {$type eq "FriendlyName"} {
			set name $value
		}
	}
	return "$name"
}

ad_proc -public intranet_termbase::multiterm::tb_company {
	-tb_path
} {
	Return the company_id for the termbase if naming convention is upheld

	Otherwise return ""
} {
	set company_id ""
	set filename [file rootname [file tail $tb_path]]
	if {[string match "TB__*" $filename]} {
		set company_path [string range $filename 4 end]
		set company_id [db_string company "select company_id from im_companies where company_path = :company_path" -default ""]
	}
	return $company_id
}


ad_proc -public intranet_termbase::multiterm::import_from_sdltb {
	-tb_name
	-project_id
	{-overwrite_p "0"}
	{-tmp_filename ""}
} {
	Extract the Termbase into the folder specified with xml_folder

	@param tb_name Name of the TB to analyse. Usually is the fineame without the ".sdltb". Needs to be in the root folder of the project
	@param project_id ProjectID for the sdltb to analyse.
} {

	im_trans_trados_project_info -project_id $project_id -array_name project_info

	set trados_dir $project_info(trados_dir)
	set trados_dir_win $project_info(trados_dir_win)
	set project_nr $project_info(project_nr)
	set company_id $project_info(company_id)

	# Copy the tmp_filename to the folder
	if {$tmp_filename ne ""} {
		file copy -force $tmp_filename ${trados_dir}/TB/${tb_name}.sdltb
	}

	# ---------------------------------------------------------------
	# Prepare the config file
	# ---------------------------------------------------------------
	ns_log Notice "Starting the Extraction of $tb_name in $project_nr"

	set trados_config "Mode#ExportTBXML
ExportSDLTB#${trados_dir_win}\\TB\\${tb_name}.sdltb
ExportXML#${trados_dir_win}\\TB\\${tb_name}.xml
StudioProjectFolder#$trados_dir_win
ProjectFile#$trados_dir_win\\${project_nr}.sdlproj"

	set config_file "${trados_dir}/TB_extraction.config"
	set file [open $config_file w+]
	fconfigure $file -encoding "utf-8"
	puts $file $trados_config
	flush $file
	close $file

	ns_log Notice "Starting extraction into XML for Termbase $tb_name"

	set duration [im_trans_trados_console_exec -config_file "${trados_dir_win}\\TB_extraction.config"]

	ns_log Notice "Trados Termbase extraction in $trados_dir_win took $duration for $trados_dir/TB/$tb_name.xml"

	set termbase_infos ""
	if {[file exists ${trados_dir}/TB/${tb_name}.xml]} {
		set termbase_infos [intranet_termbase::multiterm::xml::import_termbase -xml_path "${trados_dir}/TB/${tb_name}.xml" -tb_path "${trados_dir}/TB/${tb_name}.sdltb" -company_id $company_id -overwrite_p $overwrite_p -project_id $project_id]
	}

	set termbase_html "<html><body><h2>Uploaded termbase $tb_name</h2><ul>"
	foreach termbase_info $termbase_infos {
	    ns_log Notice "$termbase_info"
	    append termbase_html "<li>$termbase_info</li>"
	}
	append termbase_html "</ul></body></html>"
	set email [party::email -party_id [ad_conn user_id]]
	acs_mail_lite::send -from_addr $email -to_addr $email -send_immediately -subject "Update termbase" -body $termbase_html -mime_type "text/html"
	return $termbase_infos
	# file delete ${trados_dir}/${tb_name}.xml
}

ad_proc -public intranet_termbase::multiterm::export_into_sdltb {
	-tb_name
	-project_id
} {
	Import Data from the termbase database into an sdltb

	@param tb_name Name of the TB to analyse. Usually is the fineame without the ".sdltb" Needs to be in the root folder of the project.
	@param project_id ProjectID for the sdltb to analyse.
} {

	# Copy the project Dir and prep the working dir
	im_trans_trados_project_info -project_id $project_id -array_name project_info

	set trados_dir $project_info(trados_dir)
	set trados_dir_win $project_info(trados_dir_win)
	set project_nr $project_info(project_nr)

	intranet_termbase::multiterm::export_termbase -xml_path ${trados_dir}/TB/${tb_name}.xml -project_id $project_id -company_id $project_info(company_id)

	# ---------------------------------------------------------------
	# Prepare the config file
	# ---------------------------------------------------------------
	ns_log Notice "Saving $tb_name from the database into ${tb_name}.sdltb in $project_nr"

	set trados_config "Mode#ImportTBXML
ImportSDLTB#${trados_dir_win}\\TB\\${tb_name}.sdltb
ImportXML#${trados_dir_win}\\TB\\${tb_name}.xml
StudioProjectFolder#$trados_dir_win
ProjectFile#$trados_dir_win\\${project_nr}.sdlproj"

	set config_file "${trados_dir}/TB_import.config"
	set file [open $config_file w+]
	fconfigure $file -encoding "utf-8"
	puts $file $trados_config
	flush $file
	close $file

	ns_log Notice "Starting console action for saving Termbase $tb_name"

	set duration [im_trans_trados_console_exec -config_file "${trados_dir_win}\\TB_import.config"]

	ns_log Notice "Trados Termbase saving in $trados_dir_win took $duration"



	# file delete ${trados_dir}/${tb_name}.xml
}


ad_proc -public intranet_termbase::multiterm::update_termbase {
	-termbase_id
	-project_id
	-project_tb_file
	-overwrite:boolean
} {
	Update the Termbase. this assumes the termbase has been updated in the project folder and needs to be merged
	back with the rest.
} {
	
	set tb_name [intranet_termbase::termbase_name -termbase_id $termbase_id]

	# ---------------------------------------------------------------
	# CHECK IF WE NEED TO UPDATE
	# ---------------------------------------------------------------
	set update_p 1

	set termbase_file_id [db_string file_item_id "select max(item_id) from cr_items where parent_id = :termbase_id and storage_type = 'file'" -default ""]
	if {$termbase_file_id ne ""} {
		# We do have an existing termbase_file, check the hashsums for the latest revision
		set cr_path [content::revision::get_cr_file_path -revision_id [content::item::get_live_revision -item_id $termbase_file_id]]
		set termbase_file_hash [fs::torrent::get_hashsum -filename $cr_path]
		set project_tb_file_hash [fs::torrent::get_hashsum -filename $project_tb_file]
		ns_log Notice "HASH:: $termbase_file_hash :: $project_tb_file_hash :: $cr_path :: $project_tb_file"
		if {$termbase_file_hash eq $project_tb_file_hash} {
			set update_p 0
			ns_log Notice "Ignoring update as they have the same hash sum"
		}
	}

	if {$overwrite_p || $update_p} {
		# First export Termbase into XML and merge
		set termbase_infos [intranet_termbase::multiterm::import_from_sdltb -tb_name $tb_name -project_id $project_id -overwrite_p $overwrite_p]
	
		if {0} {
			# ignore the backmerge for the time being
		
			# Now export the resulting (merged) termbase into a new sdltb
			intranet_termbase::multiterm::export_into_sdltb -tb_name $tb_name -project_id $project_id
		
			# Store the resulting file with the termbase
			set mime_type "application/trados_tb"
			set file_size [file size $project_tb_file]
			set filename [util_text_to_url  -text $tb_name -replacement "_"]
			
			if {$termbase_file_id eq ""} {
				set revision_id [cr_import_content  -storage_type "file" -title $tb_name -package_id "" $termbase_id $project_tb_file $file_size $mime_type $filename]
			} else {
				set revision_id [cr_import_content  -storage_type "file" -title $tb_name -package_id "" -item_id $termbase_file_id $termbase_id $project_tb_file $file_size $mime_type $filename]
			}	
			content::item::set_live_revision -revision_id $revision_id
			
			set cr_path [content::revision::get_cr_file_path -revision_id [content::item::get_live_revision -item_id $termbase_file_id]]
			set termbase_file_hash [fs::torrent::get_hashsum -filename $cr_path]
			set project_tb_file_hash [fs::torrent::get_hashsum -filename $project_tb_file]
			ns_log Notice "HASH after update:: $termbase_file_hash :: $project_tb_file_hash :: $cr_path :: $project_tb_file"
		}
	}
}
