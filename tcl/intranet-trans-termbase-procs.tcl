# Initial procs

namespace eval intranet_termbase {}

ad_proc -public intranet_termbase::description_types {
	-array_name
} {
	Returns a list of description_types and sets the array
} {
	upvar 1 $array_name category_array
	if {[info exists category_array]} {
		unset category_array
	}
	
	# ---------------------------------------------------------------
	# 	Get the description categories
	# ---------------------------------------------------------------
	set desc_cats [list]
	
	db_foreach desc_cats "select category, category_id from im_categories where category_type = 'Intranet Termbase Description Type'" {
		lappend desc_cats $category
		set category_array($category) $category_id
	}
	return $desc_cats
}

ad_proc -public intranet_termbase::user_id {
	-termbase_user
} {
	Returns the user_id
} {
	set user_id [acs_user::get_by_username -username "$termbase_user"]
	if {"" eq $user_id} {
		array set creation_info [auth::create_user \
			-user_id $user_id \
			-verify_password_confirm \
			-username $termbase_user \
			-email "${termbase_user}@foo.com" \
			-first_names "$termbase_user" \
			-last_name "TB USER" \
			-screen_name ""]
		return $creation_info(user_id)
	} else {
		return $user_id
	}
}


ad_proc -public intranet_termbase::import_concept {
	-concept_nr
	-termbase_id
	-creation_user
	-creation_date
	-modifying_user
	-modification_date
	-project_start_date
	{-overwrite_p 0}
} {
	Import the concept
	
	@return list of concept_id and import_term_p to indicate whether we actually need to import the terms
	@param origination_date Date in YYYY-MM-DD HH24:MI:SS format
	
	@param description_list A list of lists with the language_id description_type_id and description. 
} {
	
	set concept_infos [list]
	set update_p [db_0or1row new "select concept_id, to_char(modification_date, 'YYYY-MM-DD HH24:MI:SS') as po_modification_date, to_char(creation_date,'YYYY-MM-DD HH24:MI:SS') as po_creation_date from acs_objects o, im_tb_concepts itc where itc.concept_nr = :concept_nr and itc.termbase_id = :termbase_id and o.object_id = itc.concept_id"]
	
	if {$update_p} {
		if {$overwrite_p} {
			set mode "overwrite"
			# Do a full update
			db_dml modifcation "update acs_objects set modifying_user = :modifying_user where object_id = :concept_id"
			db_dml modification "update im_tb_concepts set modification_date = :modification_date, concept_status_id = 4351 where concept_id = :concept_id"
		} else {
			if {$po_creation_date eq $creation_date} {
				# Same creation date, so it is the same version that was downloaded
				if {$po_modification_date eq $modification_date} {
					# Creation and modification date are the same, so nothing changed
					set mode "ignore"
				} else {
					# Check if the po_modification_date is newer then the project start date.
					# If yes, the modification was done after the project was created and we have to 
					# Deal with a newer entry
					if {$modification_date > $project_start_date} {
						# We have something changed in this project. Now figure out if the po_modification_date 
						# is newer then the project_start_date. If this is the case, it was modified outside the 
						# Project as well and we have a different concept
						if {$po_modification_date > $project_start_date} {
							set mode "different"
						} else {
							set mode "update_own"
						}
					} else {
						# Nothing changed in this project, so ignore
						set mode "ignore"
					}
				}
			} else {
				# We received the same concept_nr with different creation dates
				# This means the concept was changed and we have to deal with 
				# Different concepts
				set mode "different"
			}
		}
		if {$mode ne "ignore"} {
			lappend concept_infos "$mode concept $concept_nr with old_date $po_modification_date and new date $modification_date"
		}
	} else {
		set concept_id [db_string new_concept "select im_tb_concept__new(null,:creation_date,:creation_user,:modification_date,:modifying_user,null,:termbase_id,:concept_nr) from dual"]
		set mode "new"
		lappend concept_infos "$mode concept on $concept_nr from $creation_date"
	}

	return [list $concept_id $mode $concept_infos]
}

ad_proc -public intranet_termbase::import_descriptions {
	{-concept_id ""}
	{-term_id ""}
	{-mode ""}
	-description_list
} {
	Import the descriptions. Delete if necessary
} {
	set description_infos [list]
	foreach desc_keyl $description_list {
		set desc_keys [keylkeys desc_keyl]
		foreach key $desc_keys {
			set $key [keylget desc_keyl $key]
		}

		# set the user and date 
		if {![info exists creation_user]} {set creation_user [ad_conn user_id]}
		if {![info exists creation_date]} {set creation_date "now()"}
		switch $mode {
			new - overwrite {
				# We delete all descriptions associated
				if {$concept_id ne ""} {
					set old_description_ids [db_list old_nums "select description_id from im_tb_descriptions where concept_id = :concept_id and description_nr = :description_nr and language_id = :language_id"]
				} elseif {$term_id ne ""} {
					set old_description_ids [db_list old_nums "select description_id from im_tb_descriptions where term_id = :term_id and description_nr = :description_nr and language_id = :language_id"]
				}
				
				foreach old_desc_id $old_description_ids {
					db_1row delete "select im_tb_description__delete(:old_desc_id) from dual"
				}
				
				set description_id [db_string new_description "select im_tb_description__new(null,:creation_date,:creation_user,null,:concept_id,:term_id,:language_id,:description_nr,:description,:description_type_id) from dual"]
 				db_dml update "update acs_objects set last_modified = :last_modified, modifying_user = :modifying_user where object_id = :description_id"
 				lappend description_infos "New $description of type $description_nr with ID $description_id. Deleted the following IDs in the process: $old_description_ids"
			}
			update_own {
				if {![info exists modifying_user]} {set modifying_user [ad_conn user_id]}
				if {![info exists last_modified]} {set last_modified "now()"}
				if {$language_id eq ""} {
					set lang_sql "and language_id is null"
				} else {
					set lang_sql "and language_id = :language_id" 	
				}

				# We only add new ones and update the own ones
				# First delete the own ones as we have a new one
				if {$concept_id ne ""} {
					set old_description_id [db_string old_nums "select description_id from im_tb_descriptions td, acs_objects o where o.object_id = td.description_id and concept_id = :concept_id and description_nr = :description_nr and modifying_user = :modifying_user and last_modified<:last_modified $lang_sql" -default ""]
					set exists_p [db_string exist "select 1 from im_tb_descriptions where concept_id = :concept_id and description_nr = :description_nr $lang_sql limit 1" -default 0]
				} elseif {$term_id ne ""} {
					set old_description_id [db_string old_nums "select description_id from im_tb_descriptions td, acs_objects o where o.object_id = td.description_id and term_id = :term_id and description_nr = :description_nr and modifying_user = :modifying_user and last_modified < :last_modified $lang_sql" -default ""]
					set exists_p [db_string exist "select 1 from im_tb_descriptions where term_id = :term_id and description_nr = :description_nr $lang_sql limit 1" -default 0]
				}

				if {!$exists_p && $old_description_id eq ""} {
					set description_id [db_string new_description "select im_tb_description__new(null,:creation_date,:creation_user,null,:concept_id,:term_id,:language_id,:description_nr,:description,:description_type_id) from dual"]
					db_dml update "update acs_objects set last_modified = :last_modified, modifying_user = :modifying_user where object_id = :description_id"
					lappend description_infos "New $description of type $description_nr with ID $description_id for concept $concept_id and term $term_id"
				} else {
					if {$old_description_id eq ""} {
						ns_log Debug "intranet_termbase::import_descriptions - Ignoring existing description $description $description_nr"
					} else {
						db_1row old_desc "select description as old_desc from im_tb_descriptions where description_id = :old_description_id"
						lappend description_infos "Update OWN existing description $old_desc with $description. Type $description_nr with ID $description_id"
						# Update the old description
						db_dml update_desc "update im_tb_descriptions set description = :description, description_type_id = :description_type_id, language_id = :language_id where description_id = :old_description_id"
						db_dml update_objs "update acs_objects set last_modified = :last_modified where object_id = :old_description_id"
					} 
				}
			}
			default {
				# Do nothing
			}
		}
	}
	foreach description_info $description_infos {
		ns_log Debug "$description_info"
	}
	return $description_infos
}

ad_proc -public intranet_termbase::import_term {
	-term_nr
	-term
	-concept_id
	-creation_user
	-creation_date
	-modifying_user
	-modification_date
	-language_id
	-project_start_date
	{-mode ""}
} {
	Import the Term
} {
	set term_infos [list]
	set language_pretty [im_name_from_id $language_id]
	
	set update_p [db_0or1row new "select term_id, itt.term as old_term, to_char(itt.modification_date, 'YYYY-MM-DD HH24:MI:SS') as po_modification_date, to_char(o.creation_date,'YYYY-MM-DD HH24:MI:SS') as po_creation_date, itc.concept_nr from acs_objects o, im_tb_concepts itc, im_tb_terms itt where itc.concept_id = itt.concept_id and itt.term_nr = :term_nr and itt.language_id = :language_id and o.object_id = itt.term_id and itt.concept_id = :concept_id"]
	
	if {$update_p eq 0 || $mode eq "new"} {
		# We don't have any terms to worry about, just create them
		db_1row concept "select concept_nr from im_tb_concepts where concept_id =:concept_id" 
		set term_id [db_string term_id "select im_tb_term__new(null,:creation_date,:creation_user,:modification_date,:modifying_user,null,:concept_id,:language_id,:term_nr,:term) from dual" -default ""]
		lappend term_infos "Created new Term $term ($term_nr) in concept $concept_nr for language $language_pretty"
	} else {
		# We have an existing old term, get the info
		if {$mode eq "overwrite"} {
			lappend term_infos "Overwriting term in $concept_nr for $language_pretty. Replace $old_term ($po_modification_date) with $term ($modification_date)"
			# Update the term
			db_dml update_term "update im_tb_terms set term = :term, modification_date = :modification_date where term_id = :term_id"
			db_dml modifcation "update acs_objects set modifying_user = :modifying_user where object_id = :term_id"
		} elseif {$mode eq "update_own"} {
			if {$po_creation_date eq $creation_date} {
				# Same creation date, so it is the same version that was downloaded
				if {$po_modification_date eq $modification_date} {
					# Creation and modification date are the same, so nothing changed
				} else {
					# Check if the po_modification_date is newer then the project start date.
					# If yes, the modification was done after the project was created and we have to 
					# Deal with a newer entry
					if {$modification_date > $project_start_date} {
						# We have something changed in this project. Now figure out if the po_modification_date 
						# is newer then the project_start_date. If this is the case, it was modified outside the 
						# Project as well and we have a different concept
						if {$po_modification_date > $project_start_date} {
							if {$old_term ne $term} {
								# We need to add the term as we don't know if it was changed
								set new_term_nr [db_string max_concept "select max(term_nr::integer) from im_tb_terms where concept_id = :concept_id and language_id = :language_id"]
								set new_term_nr [expr $new_term_nr +1]

								set term_id [db_string term_id "select im_tb_term__new(null,:creation_date,:creation_user,:modification_date,:modifying_user,null,:concept_id,:language_id,:new_term_nr,:term) from dual" -default ""]
								lappend term_infos "Different Term, therefore created new Term $term ($term_nr) in concept $concept_nr for language $language"
							}
						} else {
							# We can update, as it was modified in this project
							db_dml update_term "update im_tb_terms set term = :term, modification_date = :modification_date where term_id = :term_id"
							lappend term_infos "Updating term in $concept_nr for $language. Replace $old_term ($po_modification_date) with $term ($modification_date)"
						}	
					} 			
				}
			}
		}
	}

	return [list $term_id $term_infos]
}

ad_proc -public intranet_termbase::termbase_component {
	{-company_id ""}
	{-project_id ""}
	{-return_url ""}
} {
	Display the TBs of a company or project
} {	
	if {$company_id ne ""} {
		set params [list  [list base_url "/intranet-trans-termbase/"]  [list company_id $company_id] [list return_url [im_biz_object_url $company_id]]]
	} elseif {$project_id ne ""} {
		set params [list  [list base_url "/intranet-trans-termbase/"]  [list project_id $project_id] [list return_url [im_biz_object_url $project_id]]]
	}

	if {[exists_and_not_null params]} {
		set result [ad_parse_template -params $params "/packages/intranet-trans-termbase/lib/termbase-list"]	
	} else {
		set result ""
	}
}

ad_proc -public intranet_termbase::termbase_id {
	-company_id
	{-final_company_id ""}
} {
	Return the termbase id for the company

	Termbases are not language dependend and contain all languages.
	Find the termbase of the final customer, then the customer and if not present use the default one from the internal company.

} {
	set termbase_id ""
	# Check if we have a final customer's termbase
	if {$final_company_id ne ""} {
		set termbase_id [db_string fc_termbase "select max(termbase_id) from im_termbases where company_id = :final_company_id" -default ""]
	}

	# Check if we have one for the customer
	if {$termbase_id eq ""} {
		set termbase_id [db_string termbase "select max(termbase_id) from im_termbases where company_id = :company_id" -default ""]
	}

	# if the file does not exist, use the default internal termbase
	if {$termbase_id eq ""} {
		set termbase_id [db_string internal_tb "select max(termbase_id) from im_termbases where company_id = [im_company_internal]" -default ""]
	}

	return "$termbase_id"	
}

ad_proc -public intranet_termbase::publish_termbase {
	-tb_path
	{-final_company_id ""}
	{-company_id ""}
	{-termbase_id ""}
} {
	Publish the termbase file to the tb_path for a company. Filename is based on the tb_name
	
	@param company_id CompanyID for the termbase
	@param tb_path Path in which the termbase file will be saved. 
	@return Full path to the termbase file
} {
	set tb_file_path ""
	if {$termbase_id eq ""} {
		set termbase_id [intranet_termbase::termbase_id -company_id $company_id -final_company_id $final_company_id]
	}
	if {$termbase_id ne ""} {
		set termbase_file_id [db_string file_item_id "select item_id from cr_items where parent_id = :termbase_id and storage_type = 'file'" -default ""]
		if {$termbase_file_id ne ""} {
			set tb_name [intranet_termbase::termbase_name -termbase_id $termbase_id]
			set cr_path [content::revision::get_cr_file_path -revision_id [content::item::get_live_revision -item_id $termbase_file_id]]
			file copy -force $cr_path "${tb_path}/${tb_name}.sdltb"
		}
	}
	return $tb_file_path
}

ad_proc -public intranet_termbase::termbase_name {
	{-termbase_id ""}
	{-company_id ""}
	{-final_company_id ""}
} {
	Return the name of the termbase
} {
	if {$termbase_id eq ""} {
		set termbase_id [intranet_termbase::termbase_id -company_id $company_id -final_company_id $final_company_id]
	}
	
	if {$termbase_id ne ""} {
		set termbase_name [db_string termbase_name "select termbase_name from im_termbases where termbase_id = :termbase_id" -default ""]
	} {
		set termbase_name ""
	}
	return $termbase_name
}

ad_proc -public intranet_termbase::termbase_language_ids {
	-termbase_id
} {
	Returns the list of languages supported and sets the array correctly
} {
	# Try the languages first from the terms in the termbases.
	set language_ids [db_list languages "select distinct language_id from im_tb_terms tt, im_tb_concepts tc where tt.concept_id = tc.concept_id and tc.termbase_id = :termbase_id"]
	if {$language_ids eq ""} {
		set language_ids [db_list languages "select language_id from im_tb_languages where termbase_id = :termbase_id"]
	}

	return $language_ids
}

ad_proc -public intranet_termbase::tb_languages {
	-termbase_id
	-array_name
} {
	Returns the languages for the termbase_id
} {

	upvar 1 $array_name tb_array
	if {[info exists tb_array]} {
		unset tb_array
	}
	
	set language_ids [intranet_termbase::termbase_language_ids -termbase_id $termbase_id]
		
	set supported_locales [list]
	
	foreach language_id $language_ids {
		db_1row language_info "select lower(category) as locale, aux_string1 as language from im_categories where category_id = :language_id"
		set tb_array($locale) $language
		lappend supported_locales $locale
	}
	return $supported_locales
}

ad_proc -public intranet_termbase::main_language {
	-termbase_id
} {
	Return the most often used language
} {
	set language_ids [db_list languages "select language_id, count(term_id) as count from im_tb_terms tt, im_tb_concepts tc where tt.concept_id = tc.concept_id and tc.termbase_id = :termbase_id group by language_id order by count desc"]
	set main_language_id [lindex $language_ids 0]
	return $main_language_id
}
