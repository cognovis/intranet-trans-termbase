-- /packages/intranet-trans-termbase/sql/postgresql/intranet-trans-termbase-drop.sql
--
-- Copyright (C) 2016 cognovis GmbH
--
-- This program is free software. You can redistribute it
-- and/or modify it under the terms of the GNU General
-- Public License as published by the Free Software Foundation;
-- either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the
-- hope that it will be useful, but WITHOUT ANY WARRANTY;
-- without even the implied warranty of MERCHANTABILITY or
-- FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS integer AS '
DECLARE

	v_termbase	record;

BEGIN
		   for v_termbase in select termbase_id from im_termbases
           loop
       PERFORM im_termbase__delete(v_termbase.termbase_id);
           end loop;
      return 0;
END;' language 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();


drop function im_tb_description__delete(integer);
drop function im_tb_description__new(integer,timestamptz,integer,varchar,integer,integer,integer,text,text,integer);
select cog_object_type__delete('im_tb_description');
drop function im_tb_term__delete(integer);
drop function im_tb_term__new(integer,timestamptz,integer,timestamptz,integer,varchar,integer,integer,text,text);
select cog_object_type__delete('im_tb_term');
drop function im_tb_concept__delete(integer);
drop function im_tb_concept__new(integer,timestamptz,integer,timestamptz,integer,varchar,integer,text);
drop function im_tb_concept__new(integer,timestamptz,integer,timestamptz,integer,varchar,integer,integer);
select cog_rel_type__delete('im_tb_concept_concept_rel');
select cog_object_type__delete('im_tb_concept');
drop table im_tb_concepts_rels;
drop table im_tb_languages;
drop function im_termbase__delete(integer);
drop function im_termbase_name_from_id(integer);
drop function im_termbase__name(integer);
drop function im_termbase__new(integer,timestamptz,integer,varchar,text,integer);
select cog_object_type__delete('im_termbase');
delete from im_categories where category_id in (4350,4351,4352,4353);
