-- /packages/intranet-trans-termbase/sql/postgresql/intranet-trans-termbase-create.sql
--
-- Copyright (C) 2016 cognovis GmbH
--
-- This program is free software. You can redistribute it
-- and/or modify it under the terms of the GNU General
-- Public License as published by the Free Software Foundation;
-- either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the
-- hope that it will be useful, but WITHOUT ANY WARRANTY;
-- without even the implied warranty of MERCHANTABILITY or
-- FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.

-- 
-- Termbases
-- 
select acs_object_type__create_type (
		'im_termbase',           -- object_type
		'Termbase',              -- pretty_name
		'Termbases',             -- pretty_plural
		'im_biz_object',        -- supertype
		'im_termbases',          -- table_name
		'termbase_id',           -- id_column
		'im_termbase',           -- package_name
		'f',                    -- abstract_p
		null,                   -- type_extension_table
		'im_termbase__name'      -- name_method
);

insert into acs_object_type_tables (object_type,table_name,id_column)
values ('im_termbase', 'im_termbases', 'termbase_id');

create table im_termbases (
	termbase_id						integer
									constraint im_termbases_pk
									primary key
									constraint im_termbases_tb_id_fk
									references acs_objects,
	termbase_name					text,
	company_id						integer
									constraint im_termbases_company_fk
									references im_companies
);

-- added
select define_function_args('im_termbase__new','termbase_id,creation_date,creation_user,creation_ip,termbase_name,company_id,termbase_type_id,termbase_status_id');

--
-- procedure im_termbase__new
--
CREATE OR REPLACE FUNCTION im_termbase__new(
   p_termbase_id integer,
   p_creation_date timestamptz,
   p_creation_user integer,
   p_creation_ip varchar,
   p_termbase_name text,
   p_company_id integer
) RETURNS integer AS $$
DECLARE
		v_termbase_id    integer;
BEGIN
		v_termbase_id := acs_object__new (
				p_termbase_id,
				'im_termbase', -- object_type
				p_creation_date,
				p_creation_user,
				p_creation_ip,
				p_company_id
		);

		insert into im_termbases (
			termbase_id, termbase_name, company_id
		) values (
			v_termbase_id, p_termbase_name, p_company_id
		);
		return v_termbase_id;
END;
$$ LANGUAGE plpgsql;

--
-- procedure im_termbase__name
--
CREATE OR REPLACE FUNCTION im_termbase__name(
   v_termbase_id integer
) RETURNS varchar AS $$
DECLARE
		v_name          varchar;
BEGIN
		select  termbase_name
		into    v_name
		from    im_termbases
		where   termbase_id = v_termbase_id;

		return v_name;
END;
$$ LANGUAGE plpgsql;

--
-- procedure im_termbase_name_from_id
--
CREATE OR REPLACE FUNCTION im_termbase_name_from_id(
   p_termbase_id integer
) RETURNS varchar AS $$
DECLARE
		v_termbase_name  varchar;
BEGIN
		select termbase_name
		into v_termbase_name
		from im_termbases
		where termbase_id = p_termbase_id;

		return v_termbase_name;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION im_termbase__delete(
   v_termbase_id integer
) RETURNS integer AS $$
DECLARE
   v_concept record;
   v_content_item record;
BEGIN

	for v_concept in select concept_id from im_tb_concepts where termbase_id = v_termbase_id 
	loop 
		PERFORM im_tb_concept__delete(v_concept.concept_id);
	end loop;
		
	for v_content_item in select item_id from cr_items where parent_id = v_termbase_id
	loop
        PERFORM content_item__delete(v_content_item.item_id);
	end loop;

	delete from im_tb_languages where termbase_id = v_termbase_id;
	delete from im_termbases where termbase_id = v_termbase_id;

	PERFORM acs_object__delete(v_termbase_id);
	return 0;
END;
$$ LANGUAGE plpgsql;

create table im_tb_languages (
	language_id					integer
								constraint im_tb_languages_tb_id_fk
								references im_categories(category_id),
	termbase_id					integer not null
								constraint im_tb_languages_termbase_fk
								references im_termbases,
	type						text,
	lang						text
);

-- 
-- Concepts
-- 
select acs_object_type__create_type (
		'im_tb_concept',           -- object_type
		'Termbase Concept',              -- pretty_name
		'Termbase Concepts',             -- pretty_plural
		'im_biz_object',        -- supertype
		'im_tb_concepts',          -- table_name
		'concept_id',           -- id_column
		'im_tb_concept',           -- package_name
		'f',                    -- abstract_p
		null,                   -- type_extension_table
		null      -- name_method
);

insert into acs_object_type_tables (object_type,table_name,id_column)
values ('im_tb_concept', 'im_tb_concepts', 'concept_id');


create table im_tb_concepts (
	concept_id						integer
									constraint im_tb_concepts_pk
									primary key
									constraint im_tb_concepts_tb_id_fk
									references acs_objects,
	termbase_id						integer not null
									constraint im_tb_concepts_termbase_fk
									references im_termbases,
	concept_nr						integer,
	modification_date				timestamptz, -- External modification_date
	concept_status_id				integer
									constraint im_tb_concepts_status_fk
									references im_categories
);

create unique index im_tb_concepts_nr_un on im_tb_concepts (concept_nr, termbase_id);

update acs_object_types set
		status_type_table = 'im_tb_concepts',
		status_column = 'concept_status_id'
where object_type = 'im_tb_concept';

SELECT im_category_new (4350,'Original', 'Intranet Trans TB Status');
SELECT im_category_new (4351,'Updated', 'Intranet Trans TB Status');
SELECT im_category_new (4352,'Replaced', 'Intranet Trans TB Status');
SELECT im_category_new (4353,'Deleted', 'Intranet Trans TB Status');


-- added
select define_function_args('im_tb_concept__new','concept_id,creation_date,creation_user,modification_date,modifying_user,creation_ip,termbase_id,concept_nr');

--
-- procedure im_tb_concept__new
--
CREATE OR REPLACE FUNCTION im_tb_concept__new(
   p_concept_id integer,
   p_creation_date timestamptz,
   p_creation_user integer,
   p_modification_date timestamptz,
   p_modifying_user integer,
   p_creation_ip varchar,
   p_termbase_id integer,
   p_concept_nr integer
) RETURNS integer AS $$
DECLARE
		v_concept_id    integer;
BEGIN
		v_concept_id := acs_object__new (
				p_concept_id,
				'im_tb_concept',
				p_creation_date,
				p_creation_user,
				p_creation_ip,
				p_termbase_id
		);

		insert into im_tb_concepts (
				concept_id, concept_nr, termbase_id, modification_date, concept_status_id
		) values (
				v_concept_id, p_concept_nr, p_termbase_id, p_modification_date, 4350
		);
		update acs_objects set modifying_user = p_modifying_user where object_id = v_concept_id;
		return v_concept_id;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION im_tb_concept__delete(
   v_concept_id integer
) RETURNS integer AS $$
DECLARE
   v_term record;
   v_description record;
BEGIN

	for v_term in select term_id from im_tb_terms where concept_id = v_concept_id 
	loop 
		PERFORM im_tb_term__delete(v_term.term_id);
	end loop;

	-- Delete all descriptions
	for v_description in select description_id from im_tb_descriptions where concept_id = v_concept_id 
	loop 
		PERFORM im_tb_description__delete(v_description.description_id);
	end loop;

	delete from     im_tb_concepts
	where           concept_id = v_concept_id;

	PERFORM acs_object__delete(v_concept_id);

	return 0;
END;
$$ LANGUAGE plpgsql;


create or replace function inline_0 ()
returns integer as '
declare
		v_count         integer;
begin
		select count(*) into v_count from acs_rel_types where
			  rel_type = ''im_tb_concepts_concept_rel'' ;

		IF v_count > 0 THEN return 1; END IF;

		create table im_tb_concepts_rels (
			rel_id                 integer
									constraint im_tb_concept_rel_fk
									references acs_rels (rel_id)
									constraint im_tb_concept_rel_pk
									primary key,
			rel_type_id      		integer not null
									constraint im_concept_rel_type_fk
									references im_categories
		);

		perform acs_rel_type__create_type (
		''im_tb_concept_concept_rel'',         -- relationship (object) name
			''TB Concept Relation'',          -- pretty name
			''TB Concept Relations'',         -- pretty plural
			''relationship'',                 -- supertype
			''im_tb_concepts_rels'',        -- table_name
			''rel_id'',                       -- id_column
			''im_tb_concept_concept_rel'',         -- package_name
			''im_tb_concept'',                   -- object_type_one
			''member'',                       -- role_one
			0,                              -- min_n_rels_one
			null,                           -- max_n_rels_one
			''im_tb_concept'',                       -- object_type_two
			''member'',                       -- role_two
			0,                              -- min_n_rels_two
			null                            -- max_n_rels_two
		);

		RETURN 0;

end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

-- 
-- terms
-- 
select acs_object_type__create_type (
		'im_tb_term',           -- object_type
		'Termbase term',              -- pretty_name
		'Termbase terms',             -- pretty_plural
		'im_biz_object',        -- supertype
		'im_tb_terms',          -- table_name
		'term_id',           -- id_column
		'im_tb_term',           -- package_name
		'f',                    -- abstract_p
		null,                   -- type_extension_table
		null      -- name_method
);

insert into acs_object_type_tables (object_type,table_name,id_column)
values ('im_tb_term', 'im_tb_terms', 'term_id');

create table im_tb_terms (
	term_id						integer
								constraint im_tb_terms_pk
								primary key
								constraint im_tb_terms_tb_id_fk
								references acs_objects,
	concept_id					integer not null
								constraint im_tb_terms_concept_fk
								references im_tb_concepts,
	term_nr						text, -- External Reference ID
	term						text,
	language_id					integer not null
		   						constraint im_tb_terms_language_fk
		   						references im_categories,
	modification_date			timestamptz, -- External modification_date
	term_status_id				integer
								constraint im_tb_terms_status_fk
								references im_categories
);

update acs_object_types set
		status_type_table = 'im_tb_terms',
		status_column = 'term_status_id'
where object_type = 'im_tb_term';

-- added
select define_function_args('im_tb_term__new','term_id,creation_date,creation_user,modification_date,modifying_user,creation_ip,concept_id,term_nr');

--
-- procedure im_tb_term__new
--
CREATE OR REPLACE FUNCTION im_tb_term__new(
   p_term_id integer,
   p_creation_date timestamptz,
   p_creation_user integer,
   p_modification_date timestamptz,
   p_modifying_user integer,
   p_creation_ip varchar,
   p_concept_id integer,
   p_language_id integer,
   p_term_nr text,
   p_term text
) RETURNS integer AS $$
DECLARE
		v_term_id    integer;
BEGIN
		v_term_id := acs_object__new (
				p_term_id,
				'im_tb_term',
				p_creation_date,
				p_creation_user,
				p_creation_ip,
				p_concept_id
		);

		insert into im_tb_terms (
				term_id, term_nr, concept_id, language_id, term, modification_date, term_status_id
		) values (
				v_term_id, p_term_nr, p_concept_id, p_language_id, p_term, p_modification_date, 4350
		);
		update acs_objects set modifying_user = p_modifying_user where object_id = v_term_id;

		return v_term_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION im_tb_term__delete(
   v_term_id integer
) RETURNS integer AS $$
DECLARE
   v_description record;
BEGIN

	-- Delete all descriptions
	for v_description in select description_id from im_tb_descriptions where term_id = v_term_id 
	loop 
		PERFORM im_tb_description__delete(v_description.description_id);
	end loop;
		
	-- Erase the terms associated with the id
	delete from     im_tb_terms
	where           term_id = v_term_id;

	PERFORM acs_object__delete(v_term_id);

	return 0;
END;
$$ LANGUAGE plpgsql;

--
-- Term Descriptions
--

select acs_object_type__create_type (
		'im_tb_description',           -- object_type
		'Termbase Description',              -- pretty_name
		'Termbase Descriptions',             -- pretty_plural
		'im_biz_object',        -- supertype
		'im_tb_descriptions',          -- table_name
		'description_id',           -- id_column
		'im_tb_description',           -- package_name
		'f',                    -- abstract_p
		null,                   -- type_extension_table
		null      -- name_method
);

insert into acs_object_type_tables (object_type,table_name,id_column)
values ('im_tb_description', 'im_tb_descriptions', 'description_id');

create table im_tb_descriptions (
	description_id				integer
								constraint im_tb_descriptions_pk
								primary key
								constraint im_tb_descriptions_tb_id_fk
								references acs_objects,
	concept_id					integer
								constraint im_tb_descriptions_concept_fk
								references im_tb_concepts,
	term_id						integer
								constraint im_tb_descriptions_terms_fk
								references im_tb_terms,
	description_nr				text, -- External Reference ID
	description					text,
	language_id					integer
		   						constraint im_tb_descriptions_language_fk
		   						references im_categories,
	description_type_id		integer not null
		   						constraint im_tb_term_descriptions_type_fk
		   						references im_categories,
	description_status_id		integer
								constraint im_tb_terms_status_fk
								references im_categories
);

update acs_object_types set
		status_type_table = 'im_tb_descriptions',
		status_column = 'description_status_id',
		type_column = 'description_type_id'
where object_type = 'im_tb_description';

-- added
select define_function_args('im_tb_description__new','description_id,creation_date,creation_user,creation_ip,concept_id,term_id,language_id,description_nr,description,description_type_id');

--
-- procedure im_tb_description__new
--
CREATE OR REPLACE FUNCTION im_tb_description__new(
   p_description_id integer,
   p_creation_date timestamptz,
   p_creation_user integer,
   p_creation_ip varchar,
   p_concept_id integer,
   p_term_id integer,
   p_language_id integer,
   p_description_nr text,
   p_description text,
   p_description_type_id integer
) RETURNS integer AS $$
DECLARE
		v_description_id    integer;
BEGIN
	if p_term_id is null then
		v_description_id := acs_object__new (
				p_description_id,
				'im_tb_description',
				p_creation_date,
				p_creation_user,
				p_creation_ip,
				p_concept_id
		);
	else
		v_description_id := acs_object__new (
				p_description_id,
				'im_tb_description',
				p_creation_date,
				p_creation_user,
				p_creation_ip,
				p_term_id
		);
	end if;
		
		insert into im_tb_descriptions (
				description_id, concept_id, term_id, language_id, description_nr, description, description_type_id, description_status_id
		) values (
				v_description_id, p_concept_id, p_term_id, p_language_id, p_description_nr, p_description, p_description_type_id, 4350
		);
		return v_description_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION im_tb_description__delete(
   v_description_id integer
) RETURNS integer AS $$
DECLARE
BEGIN

		-- Erase the terms associated with the id
		delete from     im_tb_descriptions
		where           description_id = v_description_id;

		PERFORM acs_object__delete(v_description_id);

		return 0;
END;
$$ LANGUAGE plpgsql;

SELECT im_component_plugin__new (
		null,                           -- plugin_id
		'acs_object',                   -- object_type
		now(),                          -- creation_date
		null,                           -- creation_user
		null,                           -- creation_ip
		null,                           -- context_id
		'Intranet Company Multiterm Component',        -- plugin_name
		'intranet-trans-termbase',                  -- package_name
		'right',                        -- location
		'/intranet/companies/view',      -- page_url
		null,                           -- view_name
		10,                             -- sort_order
		'intranet_termbase::termbase_component -company_id $company_id -return_url $return_url', 'lang::message::lookup "" intranet-trans-termbase.Company_Multiterms "Company Multiterms"'
);

-- Make the component readable for employees and poadmins
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS integer AS '
DECLARE

	v_object_id	integer;
	v_employees	integer;
	v_poadmins	integer;

BEGIN
	SELECT group_id INTO v_poadmins FROM groups where group_name = ''P/O Admins'';

	SELECT group_id INTO v_employees FROM groups where group_name = ''Employees'';

	SELECT plugin_id INTO v_object_id FROM im_component_plugins WHERE plugin_name = ''Intranet Company Multiterm Component'';

	PERFORM im_grant_permission(v_object_id,v_employees,''read'');
	PERFORM im_grant_permission(v_object_id,v_poadmins,''read'');


	RETURN 0;

END;' language 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

SELECT im_component_plugin__new (
		null,                           -- plugin_id
		'acs_object',                   -- object_type
		now(),                          -- creation_date
		null,                           -- creation_user
		null,                           -- creation_ip
		null,                           -- context_id
		'Intranet Project Multiterm Component',        -- plugin_name
		'intranet-trans-termbase',                  -- package_name
		'right',                        -- location
		'/intranet/projects/view',      -- page_url
		null,                           -- view_name
		10,                             -- sort_order
		'intranet_termbase::termbase_component -project_id $project_id -return_url $return_url', 'lang::message::lookup "" intranet-trans-termbase.Project_Multiterms "Project Multiterms"'
);

-- Make the component readable for employees and poadmins
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS integer AS '
DECLARE

	v_object_id	integer;
	v_employees	integer;
	v_poadmins	integer;

BEGIN
	SELECT group_id INTO v_poadmins FROM groups where group_name = ''P/O Admins'';

	SELECT group_id INTO v_employees FROM groups where group_name = ''Employees'';

	SELECT plugin_id INTO v_object_id FROM im_component_plugins WHERE plugin_name = ''Intranet Project Multiterm Component'';

	PERFORM im_grant_permission(v_object_id,v_employees,''read'');
	PERFORM im_grant_permission(v_object_id,v_poadmins,''read'');


	RETURN 0;

END;' language 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();
