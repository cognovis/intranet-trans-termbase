ad_page_contract {
	page to view termbases
	@author malte.sussdorff@cognovis.de
	@creation-date 2015-11-12
	@cvs-id $Id$
} -query {
	{orderby:optional}
} -properties {
	company_id:onevalue
	project_id:onevalue
	return_url:onevalue
}

set cancel_url "[ad_conn url]?[ad_conn query]"
set actions [list]

if {[info exists project_id]} {
	set company_id [db_string company_id "select company_id from im_projects where project_id = :project_id" -default ""]
#	set actions [list "#intranet-trans-termbase.Upload_Termbase#" [export_vars -base "/intranet-trans-termbase/import-multiterm" {project_id {return_url $cancel_url}}] "[_ intranet-trans-termbase.Upload_Termbase]"]
	set bulk_actions [list]
} else {
	set bulk_actions [list [_ intranet-core.Delete] "/intranet-trans-termbase/delete-tb" "Delete selected TB"]
	set actions [list "#intranet-trans-termbase.Upload_Termbase#" [export_vars -base "/intranet-trans-termbase/upload-multiterm" {company_id {return_url $cancel_url}}] "[_ intranet-trans-termbase.Upload_Termbase]"]
}


# ---------------------------------------------------------------
# List of all termbases
# ---------------------------------------------------------------
set elements {
	name {
		label {[_ file-storage.Name]} \
		display_template {
			<a href="@termbases.view_url@" title="\#file-storage.view_contents\#">@termbases.termbase_name@
		}
	}
	company_name {
		label {[_ intranet-core.Company]}
	}
	languages {
		label {[_ intranet-translation.Target_Language]}
		display_template {
			@termbases.languages;noquote@
		}
	}
	files {
		label {[_ intranet-trans-termbase.Termbase_file]}
		display_template {
			@termbases.files;noquote@
		}
	}
}
set return_url [util_get_current_url]

template::list::create \
	-name termbases \
	-multirow termbases \
	-key termbase_id \
	-actions $actions \
	-elements $elements \
	-bulk_actions $bulk_actions \
	-bulk_action_export_vars { return_url } \
	-bulk_action_method post
	
db_multirow -extend {view_url languages files} termbases termbase_sql {
	select im_name_from_id(company_id) as company_name, termbase_name, termbase_id from im_termbases where company_id = :company_id order by termbase_name
} {
	set languages "<ul>"
	
	set language_ids [intranet_termbase::termbase_language_ids -termbase_id $termbase_id]
	
	foreach language_id $language_ids {
		append languages "<li>[im_category_from_id $language_id]</li>"
	}
	
	append languages "</ul>"
	set view_url [export_vars -base "/intranet-trans-termbase/terms" -url {termbase_id}]
	
	set files "<ul>"
	foreach revision_id [db_list items "select live_revision from cr_items where parent_id = :termbase_id"] {
		db_1row file_title "select title, item_id from cr_revisions where revision_id = :revision_id"
		set download_url "[export_vars -base "/file-storage/file" -url {{file_id $item_id} {show_all_versions_p 1}}]"
		append files "<li><a href='$download_url'>$title</a></li>"
	}
	append files "</ul>"
}