# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    Delete a TM

    @param file_name
    @param company_id
    @param return_url

} {
   
    {termbase_id:multiple,optional ""}
    return_url:notnull
}

# ---------------------------------------------------------------
# We utterly ignore permissions for the time being.
# If wanted, we need to check how filestorage/delete.tcl handles it
# ---------------------------------------------------------------

foreach termbase $termbase_id {
    
    # Get the company and delete the file in the company folder
    set company_id [db_string company_id "select company_id from im_termbases where termbase_id = :termbase_id"]
    set filename [im_trans_trados_filename \
        -company_id $company_id \
        -extension ".sdltb"
    ]
    
    file delete -force "[im_trans_trados_folder -company_id $company_id]/$filename"
    
    # Delete the termbase files
    foreach item_id [db_list items "select item_id from cr_items where parent_id = :termbase"] {
        content::item::delete -item_id $item_id
    }
    db_0or1row delete "select im_termbase__delete(:termbase) from dual"
}
ad_returnredirect $return_url
