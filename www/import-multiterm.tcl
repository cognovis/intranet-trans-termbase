ad_page_contract {
    page to import a termbase
        
    @author malte.sussdorff@cognovis.de
    @creation-date 2015-11-12
    @cvs-id $Id$
} {
    project_id:integer,optional
    company_id:integer,optional
    return_url:optional
}

set user_id [ad_conn user_id]

if {[info exists project_id]} {
	set company_id [db_string company_id "select company_id from im_projects where project_id = :project_id" -default ""]
}

if {![exists_and_not_null company_id]} {
	set company_id ""
	set company_name "UNKOWN"
} else {
	set company_name [im_name_from_id $company_id]
}
	

set form_id "import_tb"

ad_form -name $form_id -html { enctype multipart/form-data } -export {company_id project_id return_url} -action /intranet-trans-termbase/import-multiterm -form {
	{upload_file:file {label \#file-storage.Upload_a_file\#} {html "size 30"}}
	{update_p:text(checkbox),optional 
	   	{label "[lang::message::lookup \"\" intranet-trans-termbase.update_tb_p \"Update TB?\"]"}
	   	{help_text "[lang::message::lookup \"\" intranet-trans-termbase.update_tb_help \"If checked and the file is a TB we will try to update the existing version, otherwise we will replace it\"]"}
	   	{options {{"Yes" "1"}}}
	}
} -on_request {
	set update_p 1
} -on_submit {
    
	set tmp_filename [template::util::file::get_property tmp_filename $upload_file]
	set upload_filename [template::util::file::get_property filename $upload_file]
	
	set file_extension [file extension $upload_filename]
	if {$file_extension ne ".sdltb"} {
    	ad_return_error "Not allowed" "You need to upload a .sdltb Multiterm Termbase or a zip file"		
	}

	if { ![exists_and_not_null update_p] } {
		set update_p "0"
		set overwrite_p "1"
	} else {
		set overwrite_p 0
	}

	set termbase_name [intranet_termbase::multiterm::friendly_name -tb_path $tmp_filename]
		
	# Try to find the company_name from the filename
	if {$company_id eq ""} {
		set company_id [intranet_termbase::multiterm::tb_company -tb_path $upload_filename]
	}

	if {$company_id ne ""} {
		set termbase_id [db_string termbase "select termbase_id from im_termbases where company_id = :company_id and termbase_name = :termbase_name" -default ""]
		
		if {$termbase_id ne ""} {
	
			if {!$update_p} {
				# Delete the termbase files
				foreach item_id [db_list items "select item_id from cr_items where parent_id = :termbase_id"] {
					content::item::delete -item_id $item_id
				}
				# Delete the whole entries first
				db_1row delete "select im_termbase__delete(:termbase_id) from dual"
				set termbase_id [intranet_termbase::multiterm::create_termbase -tb_path $tmp_filename -company_id $company_id]
			} 
		} else {
			set termbase_id [intranet_termbase::multiterm::create_termbase -tb_path $tmp_filename -company_id $company_id]
		}
		
	    if {[exists_and_not_null project_id]} {
			# We can update the terms in the project
			intranet_termbase::multiterm::import_from_sdltb -tb_name [intranet_termbase::termbase_name -termbase_id $termbase_id] -project_id $project_id -overwrite_p $overwrite_p -tmp_filename $tmp_filename
		}
			
		# Store the file along with the termbase
		set mime_type [template::util::file::get_property mime_type $upload_file]
		set tmp_size [file size $tmp_filename]
		set filename [util_text_to_url  -text ${upload_filename} -replacement "_"]
		
		set item_id [db_string item "select max(item_id) from cr_items where parent_id = :termbase_id" -default ""]
		if {$item_id eq ""} {
			set revision_id [cr_import_content  -storage_type "file" -title $upload_filename -package_id "" $termbase_id $tmp_filename $tmp_size $mime_type $filename]
		} else {
			set revision_id [cr_import_content  -storage_type "file" -title $upload_filename -package_id "" -item_id $item_id $termbase_id $tmp_filename $tmp_size $mime_type $filename]
		}	
		content::item::set_live_revision -revision_id $revision_id
		
	} else {
		ad_returnerror "NO company" "Could not find company_id for $tmp_filename :: $upload_filename"
	}
		
} -after_submit {
	# Now we can delete the file
	file delete -force $tmp_filename

    if {[exists_and_not_null return_url]} {
		ad_returnredirect $return_url
    } else {
		ad_returnredirect "./?company_id=$company_id"
    }
    ad_script_abort

}

ad_return_template
