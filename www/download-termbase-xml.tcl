# /packages/intranet-trans-trados/trados/download-xml.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Purpose: form to quickly add a translation project
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-08-05
} {
    termbase_id:integer
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]
set company_path [db_string tb_name "select company_path from im_termbases t, im_companies c where t.company_id = c.company_id and termbase_id = :termbase_id"]
set filename "TB__${company_path}"

# write the file to disk
set return_xml "/tmp/${filename}.xml"
set file [open $return_xml w]
fconfigure $file -encoding "utf-8"
puts $file [intranet_termbase::multiterm::termbase_xml -termbase_id $termbase_id]
flush $file
close $file

# Return the file
 set outputheaders [ns_conn outputheaders]
 ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"${filename}.xml\""
ns_returnfile 200 application/xml $return_xml