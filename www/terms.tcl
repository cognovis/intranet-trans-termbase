ad_page_contract { 
	List all terms of a termbase
	
	@author malte.sussdorff@cognovis.de
} {
	termbase_id:integer 
}

# ---------------------------------------------------------------
# List of all termbases
# ---------------------------------------------------------------
set elements [list]
lappend elements concept_nr {
	label {[_ intranet-trans-termbase.ConceptNr]} \
	display_template "@concepts.concept_nr;noquote@"
}

lappend elements modification_date {
	label {[_ intranet-core.Date]}
}

set language_ids [db_list languages "select language_id, count(term_id) as count from im_tb_terms tt, im_tb_concepts tc where tt.concept_id = tc.concept_id and tc.termbase_id = :termbase_id group by language_id order by count desc"]

set extend_variables [list]
foreach language_id $language_ids {
	lappend elements terms_$language_id [list \
		label "[im_category_from_id $language_id]" \
		display_template "@concepts.terms_$language_id;noquote@" \
	]
	lappend extend_variables "terms_$language_id"
}

template::list::create \
	-name concepts \
	-multirow concepts \
	-key concept_id \
	-elements $elements

set main_language_id [lindex $language_ids 0]
db_multirow -extend $extend_variables concepts concept_sql {
	select itc.concept_nr, itc.modification_date, itc.concept_id from im_tb_concepts itc, im_tb_terms itt where termbase_id = :termbase_id and itc.concept_id = itt.concept_id and itc.concept_status_id in (4350,4351) and itt.language_id = :main_language_id and term_nr = '0' order by itt.term
} {
	foreach language_id $language_ids {
		set terms_$language_id "<ul>"
	}
	
	db_foreach terms "select language_id, term, term_id from im_tb_terms where concept_id = :concept_id and term_status_id in (4350,4351) order by term_nr" {
		set descriptions [db_list descs "select description from im_tb_descriptions where term_id = :term_id order by description_nr"]
		foreach description $descriptions {
			append term "&nbsp;<img src=/intranet/images/help.gif alt='$description' title='$description'>&nbsp;"
		}
		append	terms_$language_id "<li>$term</li>"
	}
	foreach language_id $language_ids {
		append terms_$language_id "</ul>"
	}
	
	set descriptions [db_list descs "select description from im_tb_descriptions where concept_id = :concept_id order by description_nr"]
	foreach description $descriptions {
		append concept_nr "&nbsp;<img src=/intranet/images/help.gif alt='$description' title='$description'>&nbsp;"
	}
}