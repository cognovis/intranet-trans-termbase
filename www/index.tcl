ad_page_contract { 
	List all termbases
	
	@author malte.sussdorff@cognovis.de
} {
	{ company_id:integer 0 } 
}

# ---------------------------------------------------------------
# List of all termbases
# ---------------------------------------------------------------
set elements {
	name {
		label {[_ file-storage.Name]} \
		display_template {
			<a href="@termbases.view_url@" title="\#file-storage.view_contents\#">@termbases.termbase_name@
		}
	}
	company_name {
		label {[_ intranet-core.Company]}
	}
	languages {
		label {[_ intranet-translation.Target_Language]}
		display_template {
			@termbases.languages;noquote@
		}
	}
	files {
		label {[_ intranet-trans-termbase.Termbase_file]}
		display_template {
			@termbases.files;noquote@
		}
	}
}
set return_url [util_get_current_url]

template::list::create \
	-name termbases \
	-multirow termbases \
	-key termbase_id \
	-elements $elements \
	-bulk_actions [list [_ intranet-core.Delete] "/intranet-trans-termbase/delete-tb" "Delete selected TB"] \
	-bulk_action_export_vars { return_url } \
	-bulk_action_method post
	
db_multirow -extend {view_url languages files} termbases termbase_sql {
	select im_name_from_id(company_id) as company_name, termbase_name, termbase_id from im_termbases order by termbase_name
} {
	set languages "<ul>"
	
	# Try the languages first from the terms in the termbases.
	set language_ids [db_list languages "select distinct language_id from im_tb_terms tt, im_tb_concepts tc where tt.concept_id = tc.concept_id and tc.termbase_id = :termbase_id"]
	if {$language_ids eq ""} {
		set language_ids [db_string languages "select language_ids from im_termbases where termbase_id = :termbase_id"]
	}
	
	foreach language_id $language_ids {
		append languages "<li>[im_category_from_id $language_id]</li>"
	}
	append languages "</ul>"
	set view_url [export_vars -base "terms" -url {termbase_id}]
	
	set files "<ul>"
	foreach revision_id [db_list items "select live_revision from cr_items where parent_id = :termbase_id"] {
		db_1row file_title "select title, item_id from cr_revisions where revision_id = :revision_id"
		set download_url "/file/$item_id/$title"
		append files "<li><a href='$download_url'>$title</a></li>"
	}
	append files "</ul>"
}