ad_page_contract {
    page to import a termbase
        
    @author malte.sussdorff@cognovis.de
    @creation-date 2015-11-12
    @cvs-id $Id$
} {
    company_id:integer
    return_url:optional
}

set user_id [ad_conn user_id]
set company_name [im_name_from_id $company_id]
	

set form_id "import_tb"

ad_form -name $form_id -html { enctype multipart/form-data } -export {company_id return_url} -action /intranet-trans-termbase/upload-multiterm -form {
	{upload_file:file {label \#file-storage.Upload_a_file\#} {html "size 30"}}
} -on_request {
	set update_p 1
} -on_submit {
    
	set tmp_filename [template::util::file::get_property tmp_filename $upload_file]
	set upload_filename [template::util::file::get_property filename $upload_file]
	
	set file_extension [file extension $upload_filename]
	if {$file_extension ne ".sdltb"} {
    	ad_return_error "Not allowed" "You need to upload a .sdltb Multiterm Termbase or a zip file"		
	}

	set termbase_name [intranet_termbase::multiterm::friendly_name -tb_path $tmp_filename]
		
	# Try to find the company_name from the filename
	if {$company_id eq ""} {
		set company_id [intranet_termbase::multiterm::tb_company -tb_path $upload_filename]
	}

	set termbase_id [db_string termbase "select termbase_id from im_termbases where company_id = :company_id and termbase_name = :termbase_name" -default ""]
		
	if {$termbase_id eq ""} {
		set termbase_id [intranet_termbase::multiterm::create_termbase -tb_path $tmp_filename -company_id $company_id]	
	}	
		
		
	# Store the file along with the termbase
	set mime_type [template::util::file::get_property mime_type $upload_file]
	set tmp_size [file size $tmp_filename]
	
	# set filename [util_text_to_url  -text ${upload_filename} -replacement "_"]
	set filename [im_trans_trados_filename \
		-company_id $company_id \
		-extension ".sdltb"
	]
		
	set item_id [db_string item "select max(item_id) from cr_items where parent_id = :termbase_id" -default ""]
	if {$item_id eq ""} {
		set revision_id [cr_import_content  -storage_type "file" -title $filename -package_id "" $termbase_id $tmp_filename $tmp_size $mime_type $filename]
	} else {
		set revision_id [cr_import_content  -storage_type "file" -title $filename -package_id "" -item_id $item_id $termbase_id $tmp_filename $tmp_size $mime_type $filename]
	}
	content::item::set_live_revision -revision_id $revision_id
	
	# Copy the file to the company folder
	file rename -force $tmp_filename "[im_trans_trados_folder -company_id $company_id]/$filename"
		
} -after_submit {
	
    if {[exists_and_not_null return_url]} {
		ad_returnredirect $return_url
    } else {
		ad_returnredirect "./?company_id=$company_id"
    }
    ad_script_abort

}

ad_return_template
